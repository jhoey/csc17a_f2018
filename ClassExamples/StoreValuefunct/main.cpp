/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on August 30, 2018, 8:09 PM
 */

#include <cstdlib>
#include <iostream>
#include <ctime>
using namespace std;

// Function prototype
void storeVal(int* p, int size);
/*
 * 
 */
int main(int argc, char** argv) {

srand(time(0));//seed random generator


 int* p = new int[10];
    storeVal(p, 10);
    
    for(int i = 0; i < 10; i++) //Loop through 10 times
    {
        cout << p[i] << endl;  // Output the number
    }
    return 0;
}

/**
 * Create a random number
 @param p
 @ parm size 
 */
void storeVal(int* p, int size)
{
    for(int i = 0; i < size; i++)
    {
        p[i] = rand() % 10 + 1;
    }
}